'use strict';

angular.module('hexaaApp', ['ui.bootstrap']).controller('OrganizationCtrl', ['$scope', '$window', '$timeout', function($scope, $window, $timeout) {
	/* service tab is active */
	$scope.active = true;

	$scope.goTo = function(path) {
		$window.location.href = path;
	};

	$scope.logout = function() {
		alert("LOGOUT!");
	};

	/* ALERTS */
	$scope.alerts = [];

	$scope.addAlert = function(type, msg) {
		$scope.alerts.push({type: type, msg: msg});
		$timeout(function() {
			$('.alert' + ($scope.alerts.length - 1)).fadeOut();
			$scope.alerts.shift();
		}, 3000);
	};

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	/*
		GUI LOGIC
	*/

	/* Roles */
	$scope.addRoleMembers = function(idx) {
		console.log('addRoleMembers');
	};

	$scope.removeRoleMembers = function(idx) {
		console.log('removeRoleMembers');
	};

	$scope.addEntitlements = function(idx) {
		console.log('addEntitlements');
	};

	$scope.removeEntitlements = function(idx) {
		console.log('removeEntitlements');
	};

	$scope.addRoleMembersToNewRole = function() {
		console.log('addRoleMembersToNewRole');
	};

	$scope.removeRoleMembersFromNewRole = function() {
		console.log('removeRoleMembersFromNewRole');
	};

	$scope.addEntitlementsToNewRole = function() {
		console.log('addEntitlementsToNewRole');
	};

	$scope.removeEntitlementsFromNewRole = function() {
		console.log('removeEntitlementsFromNewRole');
	};

	$scope.deleteRole = function(idx) {
		console.log('deleteRole: ' + idx);
	};

	$scope.addNewRole = function() {
		console.log('addNewRole');
	};

	/* Current organization */
	$scope.saveOrganization = function() {
		$scope.addAlert('success', 'Organization saved!');
		console.log('saveOrganization');
	};

	$scope.deleteOrganization = function() {
		$scope.addAlert('danger', 'Organization deleted!');
		console.log('deleteOrganization');
	};
}]);

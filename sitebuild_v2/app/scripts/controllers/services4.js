'use strict';

angular.module('hexaaApp', ['ui.bootstrap']).controller('ServiceCtrl', ['$scope', '$window', '$timeout', function($scope, $window, $timeout) {

	/* service tab is active */
	$scope.active = true;

	$scope.goTo = function(path) {
		$window.location.href = path;
	};

	$scope.logout = function() {
		alert("LOGOUT!");
	};

	/* ALERTS */
	$scope.alerts = [];

	$scope.addAlert = function(type, msg) {
		$scope.alerts.push({type: type, msg: msg});
		$timeout(function() {
			$('.alert' + ($scope.alerts.length - 1)).fadeOut();
			$scope.alerts.shift();
		}, 3000);
	};

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	/*
		GUI LOGIC
	*/

	/* Entitlements */
	$scope.addNewEntitlement = function() {
		console.log('addNewEntitlement');
	};

	$scope.deleteEntitlement = function(idx) {
		console.log('deleteEntitlement: ' + idx);
	};

	/* Current service */
	$scope.saveService = function() {
		$scope.addAlert('success', 'Service saved!');
		console.log('saveService');
	};

	$scope.deleteService = function() {
		$scope.addAlert('danger', 'Service deleted!');
		console.log('deleteService');
	};

}]);

'use strict';

angular.module('hexaaApp', ['ui.bootstrap']).controller('OrganizationCtrl', ['$scope', '$window', '$timeout', function($scope, $window, $timeout) {
	/* service tab is active */
	$scope.active = true;

	$scope.goTo = function(path) {
		$window.location.href = path;
	};

	$scope.logout = function() {
		alert("LOGOUT!");
	};

	/* ALERTS */
	$scope.alerts = [];

	$scope.addAlert = function(type, msg) {
		$scope.alerts.push({type: type, msg: msg});
		$timeout(function() {
			$('.alert' + ($scope.alerts.length - 1)).fadeOut();
			$scope.alerts.shift();
		}, 3000);
	};

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	/*
		GUI LOGIC
	*/

	/* Current organization */
	$scope.saveOrganization = function() {
		$scope.addAlert('success', 'Organization saved!');
		console.log('saveOrganization');
	};

	$scope.deleteOrganization = function() {
		$scope.addAlert('danger', 'Organization deleted!');
		console.log('deleteOrganization');
	};
}]);

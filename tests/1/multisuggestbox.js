angular.module('app', []).controller('MSBCtrl', function($scope) {
    $scope.users = [
        {id: 1, name: 'Soledad'},
        {id: 2, name: 'Lucienne'},
        {id: 3, name: 'Tasha'},
        {id: 4, name: 'Yahaira'},
        {id: 5, name: 'Fern'},
        {id: 6, name: 'Carleen'},
        {id: 7, name: 'Jovan'},
        {id: 8, name: 'Lady'},
        {id: 9, name: 'Jerilyn'},
        {id: 10, name: 'Catherin'},
        {id: 11, name: 'Foster'},
        {id: 12, name: 'Tarah'},
        {id: 13, name: 'Izetta'},
        {id: 14, name: 'Zenia'},
        {id: 15, name: 'Marg'},
        {id: 16, name: 'Melvin'},
        {id: 17, name: 'Wilfredo'},
        {id: 18, name: 'Warren'},
        {id: 19, name: 'See'},
        {id: 20, name: 'Myrle'}];

    $scope.test = function() {
        console.log($scope.chosenUsers);
    };
}).directive('suggestBox', function() {
        return {
        restrict: 'A',
        scope: {
            users: '=',
            model: '='
        },
        link: function(scope, elm, attr) {
            // INIT
            scope.availableUsers = scope.users;
            scope.searchUsername = "";

            scope.update = function() {
                if (scope.searchUsername === "") {
                    scope.availableUsers = scope.users;
                } else {
                    scope.availableUsers = scope.filterUsers(scope.users);
                }
            };

            scope.filterUsers = function(users) {
                var availableUsers = [];
                angular.forEach(users, function(user) {
                    if (user.name.indexOf(scope.searchUsername) != -1) {
                        this.push(user);
                    }
                }, availableUsers);
                return availableUsers;
            };
        },
        template: 
        '<input type="text" ng-model="searchUsername" placeholder="search for users" ng-model-onblur ng-change="update()" class="searchUser" />' +
        '<select multiple ng-multiple="true" class="selectUser" ng-model="model" ng-options="user.name for user in availableUsers"></select>'
    };
}).directive('ngModelOnblur', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function() {
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(elm.val());
                });         
            });
        }
    };
});
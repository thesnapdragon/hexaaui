'use strict';

describe('Service: hexaaservice', function () {

  // load the service's module
  beforeEach(module('sitebuildApp'));

  // instantiate service
  var hexaaservice;
  beforeEach(inject(function (_hexaaservice_) {
    hexaaservice = _hexaaservice_;
  }));

  it('should do something', function () {
    expect(!!hexaaservice).toBe(true);
  });

});

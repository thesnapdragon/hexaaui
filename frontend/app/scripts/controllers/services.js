'use strict';

angular.module('hexaaApp', ['ui.bootstrap']).controller('ServiceCtrl', ['$scope', '$window', '$timeout', 'HexaaService', function($scope, $window, $timeout, HexaaService) {


	/*
		MOCKUPS: these objects have to be fetched from server
	*/
	// $scope.services = [
	// 	{
	// 		managers: [
	// 			{ id: 1, name: 'Manager1' },
	// 			{ id: 2, name: 'Manager2' }
	// 		],
	// 		properties: {
	// 			name: 'Service1',
	// 			url: 'http://sp.service1.com',
	// 			samlSpEntityId: 'Service1 samlSpEntityId',
	// 			description: 'Service1 description'
	// 		},
	// 		attributeSpecifications: [
	// 			{
	// 				name: 'Attribute1',
	// 				attribute: 'attribute value'
	// 			}
	// 		],
	// 		entitlements: [
	// 			{
	// 				id: 1,
	// 				name: 'Entitlement1',
	// 				description: 'Entitlement1 description',
	// 				eduPersonEntitlement: 'Entitlement1 eduPersonEntitlement'
	// 			},
	// 			{
	// 				id: 2,
	// 				name: 'Entitlement2',
	// 				description: 'Entitlement2 description',
	// 				eduPersonEntitlement: 'Entitlement2 eduPersonEntitlement'
	// 			}
	// 		],
	// 		entitlementPacks: [
	// 			{
	// 				name: 'Entitlementpack1',
	// 				description: 'Entitlementpack1 description',
	// 				type: 'Entitlementpack1 type',
	// 				token: 'Entitlementpack1 token',
	// 				entitlements: [
	// 					{ id: 1, name: 'Entitlement1' }
	// 				]
	// 			}
	// 		]
	// 	}
	// ];

	$scope.managers = [
		{ id: 1, fedid: 'solazs@sztaki.hu' },
		{ id: 2, fedid: 'u.milan@sztaki.hu' },
		{ id: 3, fedid: 'Manager3' },
		{ id: 4, fedid: 'Manager4' }
	];

	$scope.user = {
		username: 'Member0',
		email: 'member0@example.com',
		attributes: [
			{
				name: 'Attribute1',
				attribute: 'Attribute1 value',
				isDefault: true
			},
			{
				name: 'Attribute2',
				attribute: 'Attribute2 value',
				isDefault: false
			}
		],
		entitlements: [
			{
				name: 'Entitlement1',
				description: 'Entitlement1 description',
				eduPersonEntitlement: 'Entitlement1 eduPersonEntitlement'
			},
			{
				name: 'Entitlement2',
				description: 'Entitlement2 description',
				eduPersonEntitlement: 'Entitlement2 eduPersonEntitlement'
			}
		]
	};

	/*
		INIT
	*/
	/* initial collapse status */
	$scope.isCollapsed = [
		false, false, false, false, false
	];
	/* initial selected service is the first */
	$scope.selectedService = 0;
	/* at first we dont create a service */
	$scope.creatingNewService = false;
	/* service tab is active */
	$scope.active = true;
	/* Shared object with suggest boxes */
	$scope.suggestBoxData = {};

	/* new entitlement */
	$scope.entitlementTemplate = {
		name: '',
		description: '',
		eduPersonEntitlement: ''
	};
	$scope.newEntitlement = JSON.parse(JSON.stringify($scope.entitlementTemplate));

	/* new entitlementPack */
	$scope.entitlementPackTemplate = {
		name: '',
		description: '',
		type: '',
		token: '',
		entitlements: []
	};
	$scope.newEntitlementPack = JSON.parse(JSON.stringify($scope.entitlementPackTemplate));

	/* new service */
	$scope.serviceTemplate = {
			managers: [],
			properties: {
				name: '',
				url: '',
				entityid: '',
				description: ''
			},
			attributeSpecifications: [],
			entitlements: [],
			entitlement_packs: []
	};
	$scope.newService = JSON.parse(JSON.stringify($scope.serviceTemplate));

	/*
		Handling resources
	*/

	$scope.getServices = function() {
		HexaaService.getServices().
          success(function(data, status, headers, config) {
            $scope.services = data;
            if ($scope.selectedServiceId == null) {
            	$scope.selectedServiceId = $scope.services[$scope.selectedService].id;
            	$scope.getService($scope.selectedServiceId);
            }
          }).
          error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
	};

	$scope.getService = function(id) {
		HexaaService.getService(id).
          success(function(data, status, headers, config) {
            $scope.service = data;
          }).
          error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
	};

	$scope.selectService = function(idx) {
		if (idx === -1) {
			$scope.creatingNewService = true;
		} else {
			$scope.creatingNewService = false;
			$scope.selectedService = idx;
			$scope.selectedServiceId = $scope.services[$scope.selectedService].id;
			$scope.getService($scope.selectedServiceId);
		}
	};

	$scope.collapse = function(idx) {
		$scope.isCollapsed[idx] = !$scope.isCollapsed[idx];
	};

	$scope.goTo = function(path) {
		$window.location.href = path;
	};

	$scope.logout = function() {
		alert("LOGOUT!");
	};

	/* ALERTS */
	$scope.alerts = [];

	$scope.addAlert = function(type, msg) {
		$scope.alerts.push({type: type, msg: msg});
		$timeout(function() {
			$('.alert' + ($scope.alerts.length - 1)).fadeOut();
			$scope.alerts.shift();
		}, 3000);
	};

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	/*
		GUI LOGIC
	*/

	/* Managers */
	$scope.addManagers = function() {
		angular.forEach($scope.suggestBoxData['newChosenManagers'], function(newManager) {
			var canBeAdded = true;
			angular.forEach($scope.service.managers, function(manager) {
				if (manager.id === newManager.id) {
					canBeAdded = false;
				};
			});
			if (canBeAdded) {
				this.push(newManager);
			}
		}, $scope.service.managers);
	};

	$scope.removeManagers = function() {
		angular.forEach($scope.suggestBoxData['oldChosenManagers'], function(oldManager) {
			angular.forEach($scope.service.managers, function(manager, i) {
				if (oldManager.id === manager.id) {
					this.splice(i, 1);
				}
			}, $scope.service.managers);
		});
	};

	/* Entitlements */
	$scope.addNewEntitlement = function() {
		alert("ADD NEW ENTITLEMENT!");
		$scope.service.entitlements.push($scope.newEntitlement);
		$scope.newEntitlement = JSON.parse(JSON.stringify($scope.entitlementTemplate));
	};

	$scope.deleteEntitlement = function(idx) {
		$scope.service.entitlements.splice(idx, 1);
	};

	/* Entitlementpacks */
	$scope.addEntitlements = function(idx) {
		angular.forEach($scope.suggestBoxData['newChosenEntitlements'], function(newEntitlement) {
			var canBeAdded = true;
			angular.forEach($scope.service.entitlement_packs[idx].entitlements, function(entitlement) {
				if (entitlement.id === newEntitlement.id) {
					canBeAdded = false;
				};
			});
			if (canBeAdded) {
				this.push(newEntitlement);
			}
		}, $scope.service.entitlement_packs[idx].entitlements);
	};

	$scope.removeEntitlements = function(idx) {
		angular.forEach($scope.suggestBoxData['oldChosenEntitlements'], function(oldEntitlement) {
			angular.forEach($scope.service.entitlement_packs[idx].entitlements, function(entitlement, idx2) {
				if (oldEntitlement.id === entitlement.id) {
					this.splice(idx2, 1);
				}
			}, $scope.service.entitlement_packs[idx].entitlements);
		});
	};

	$scope.addNewEntitlementPack = function() {
		alert("ADD NEW ENTITLEMENTPACK!");
		$scope.service.entitlement_packs.push($scope.newEntitlementPack);
		$scope.newEntitlementPack = JSON.parse(JSON.stringify($scope.entitlementPackTemplate));
	};

	$scope.deleteEntitlementPack = function(idx) {
		$scope.service.entitlement_packs.splice(idx, 1);
	};

	/* Current service */
	$scope.saveService = function() {
		$scope.addAlert('success', 'Service: ' + $scope.service.properties.name + ' saved!');
	};

	$scope.deleteService = function() {
		$scope.addAlert('danger', 'Service: ' + $scope.service.properties.name + ' deleted!');
		// TODO
		$scope.selectService($scope.services.length - 1);
	};

	/* New organization */
	$scope.addNewService = function() {
		$scope.addAlert('success', 'Service: ' + $scope.newService.properties.name + ' created!');
		// TODO
		$scope.newService = JSON.parse(JSON.stringify($scope.serviceTemplate));
	};

	/* 
		START
	*/

	$scope.getServices();
}]);

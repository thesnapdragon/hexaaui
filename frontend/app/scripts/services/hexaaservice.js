'use strict';

var token = '7f405970fec6903d846a61d0abc13e6d668c767ccc9f6709b2ce395e32ddffa2';

angular.module('hexaaApp').factory('HexaaService', ['$http', function($http) {
    
    var HexaaService = {
      doRequest: function(method, path, token) {
        return $http({method: method, url: path, headers: {'X-HEXAA-AUTH': token}});
      }
    }

    return {
      getServices: function() {
        return HexaaService.doRequest('GET', '/api/services.json', token);
      },
      getService: function(id) {
        return HexaaService.doRequest('GET', '/api/services/' + id + '/page.json', token);
      }
    };
  }]);
